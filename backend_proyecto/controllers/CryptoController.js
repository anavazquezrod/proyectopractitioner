const KrakenAPIKey = process.env.KRAKEN_API_KEY;// Lo sustituimos por el archivo .env
const SecretKrakenAPIKey = process.env.SECRET_KRAKEN_API_KEY;// Lo sustituimos por el archivo .env



const kraken = require('node-kraken-api');
const api = kraken({
  key: KrakenAPIKey,
  secret: SecretKrakenAPIKey,
  tier: 2
}
);

function getCrytposV1(req,res){
  console.log("GET /apitechu/V1/cryptocurrencies");

  var method = req.query.method;


  if (method == undefined){
    method = 'Balance';
  }
  console.log("Se lanza el api de kraken para consultar: "+method);
  var respuesta;

  api.call(method, function (err,data){
    console.log(data);
    if (err){
      console.log("Se ha producido un error en la consulta de criptodivisas");
      console.log(err);
      res.status(500).send(err);
    }else{
      console.log("Se envia la respuesta");
      res.status(200).send(data);
    }
  }
)

//   api.call(method, (err, data) => {
//   if (err) {console.error(err);
//   }
//   else{ console.log(data);
//     res.status(200).send(data);
//   }
// })

}

module.exports.getCrytposV1 = getCrytposV1;
