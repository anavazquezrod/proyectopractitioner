
const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuavr13ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;// Lo sustituimos por el archivo .env


//función que recupera las cuentas del usuario pasándole el id de usuario logado
function getAccountByIdV1 (req, res){
  console.log("GET /apitechu/v1/accounts/:id");

  var id = Number.parseInt(req.params.id);
  console.log("La id del usuario a buscar es " + id);
  var query ="q=" + JSON.stringify({"userid": id});
  console.log("query es" +query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if(err){
      var response = {
        "msg" : "Error obteniendo cuentas."
      }
      res.status(500);
      }else {
        if(body.length >0){
          var response = body;
        }else{
          var response = {
            "msg" : "Cuenta no encontrada"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}

function createAccountV1(req,res){
  console.log("POST /apitechu/v1/accounts");

  //Recuperamos los Parametros y montamos el objeto account a crear (se creara un objeto moviemiento de apertura)
  var newAccount ={};
  newAccount.userid = Number.parseInt(req.body.userid);
  newAccount.iban_cuenta = req.body.iban;
  newAccount.saldo = Number.parseFloat(req.body.saldo);
  newAccount.movimientos = [
    {
      "tipo_movimiento":"Ingreso",
      "concepto":"Apertura de cuenta",
      "importe":Number.parseFloat(req.body.saldo),
      "moneda":"EUR"
    }
  ]

  console.log(newAccount);

  //Insertamos el objeto account en MLab.

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created para la inserción de una nueva cuenta");


  httpClient.post("account?" + mLabAPIKey, newAccount,
  function(err, resMLab, body){
    console.log("Cuenta"+newAccount.iban+" creado en Mlab");
    res.status(201).send({"msg" : "Cuenta creado", "iban":newAccount.iban});
  }

  )
}

//Funcion que recoge las cuentas en función de los parametros de entrada
function getAccountV1 (req,res){
  console.log("GET apitechu/V1/accounts");

  //Recuperamos los atributos
  console.log(req.params)
//  console.log(req.body);
//  console.log (req);
//  var iban = req.body.iban;
var iban = req.params.iban;
  console.log ("El iban de la cuenta a recuperar es: "+ iban);

  //montamos la query en función de los parametros de entrada
  var cuenta ={};
  if (iban != undefined){

    console.log("El IBAN a buscar es " + iban);
    var query ="q=" + JSON.stringify({"iban_cuenta": iban});
    console.log("query es" +query);
}
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Client created para la consulta de cuentas por IBAN");

    httpClient.get("account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        if(err){
        var response = {
          "msg" : "Error obteniendo cuentas por iban.",

        }
        res.status(500).send(response);

        }else {
          if(body.length >0){
            var response = body[0];

          res.status(200).send(response);
          }else{
            var response = {
              "msg" : "Cuenta no encontrada",

            }
            res.status(404).send(response);

          }
        }

      }
    )


}

function updateAccountV1 (req,res){
  console.log("PUT /apitechu/v1/accounts");
  //Recuperamos los campos de la petición.
  var iban = req.body.iban;
  var saldo = Number.parseFloat(req.body.saldo);
  var movimientos = req.body.movimientos;

  console.log("La cuenta a actualizar es la que tiene el IBAN: "+iban);
  console.log("El saldo es: "+saldo);
  console.log("Se añadirá el movimiento:");
  console.log(movimientos);

  //Recuperamos la cuenta por el iban
  if (iban != undefined){

    console.log("El IBAN a buscar es " + iban);
    var query ="q=" + JSON.stringify({iban_cuenta: iban});
    console.log("query es" +query);
}else{
  var response ={"msg":"No se ha informado el iban de la cuenta a actualizar"};
  res.status(400).send(response);
}

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created para la consulta de cuentas por IBAN");

  httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if(err){
      var response = {
        "msg" : "Error obteniendo cuentas por iban.",

      }
      res.status(500).send(response);

      }else {
        if(body.length >0){
          var cuentaActualizar = body[0];
          console.log ("Se ha encontrado la cuenta");
          console.log (cuentaActualizar);
          //Añadimos el movimiento que recibimos como parametro al array de la cuenta que acabamos de recuperar.
          var longitud = cuentaActualizar.movimientos.length;
          cuentaActualizar.movimientos[longitud] = movimientos[0];
          console.log (cuentaActualizar);
          //Preparamos los campos para la actualización
          if((saldo != undefined) &&(movimientos != undefined)){
            //Montamos el body del update del campo.
            var bodyUpdated  = {"$set":{"saldo":saldo,"movimientos":cuentaActualizar.movimientos}};
            //Llamamos a la actualización
            console.log("la query a actualizar es: "+query);
            //invocamos a la actualización
            httpClient.put("account?" + query + "&" + mLabAPIKey, bodyUpdated,
              function (err2, resMLab2, body2){
                console.log("Se ha invocado la actualizacion de cuentas");
                console.log(body2);

                if (!err2){
                  var response = {
                    "msg" : "Cuenta actualizada correctamente"

                  }
                  res.status(200);
                  res.send(response);
                  }else{
                    var response = {
                      "msg" : "Internal error",

                    }
                    res.status(500);
                    res.send(response);

                  }

                }

              )


          }else{
            //DEvolvemos un error por parametros
            var response ={"msg":"No se ha informado el iban de la cuenta a actualizar"};
            res.status(400).send(response);

          }


        }else{
          var response = {
            "msg" : "Cuenta no encontrada",

          }
          res.status(404).send(response);

        }
      }

    }
  )

}

//Función que borra una cuenta pasándole como id (en el body) el IBAN
function deleteAccountV1(req, res){
  console.log("DELETE /apitechu/v1/accounts");

  var iban = req.body.iban;
  console.log("El IBAN a borrar es " + iban);
  var query ="q=" + JSON.stringify({"iban_cuenta": iban});
  console.log("query es" +query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created to delete account");

  //Montamos el body a enviar
  var bodyBorrar=[{}];

  //y las cabeceras para que sean un json
  console.log(baseMLabURL+"account?" +query+"&"+mLabAPIKey);

  httpClient.put("account?" +query+"&"+mLabAPIKey,bodyBorrar,
  function (err, resMLab, body){
    console.log(resMLab);

    if(body.removed){
      res.status(200).send({"msg" : "Cuenta borrada"})
    }else{
      res.status(400).send({"msg" : "Cuenta no borrada porque no existe"})

      }

    }
  )

}






module.exports.getAccountByIdV1 = getAccountByIdV1;
module.exports.createAccountV1 = createAccountV1;
module.exports.getAccountV1 = getAccountV1;
module.exports.updateAccountV1=updateAccountV1;
module.exports.deleteAccountV1=deleteAccountV1;
